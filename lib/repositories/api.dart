import 'package:http/http.dart' as http;
import 'package:jokeapp/models/api_responses/joke_response.dart';

class Api {
  T _parsedResponse<T>(
    http.Response response, {
    Function onSuccess,
    Function onError,
  }) {
    if (response == null) {
      return null;
    } else if (200 <= response.statusCode && response.statusCode <= 204) {
      return onSuccess == null ? null : onSuccess(response);
    } else if (400 <= response.statusCode && response.statusCode < 500) {
      return onError == null ? null : onError(response);
    }
  }

  Future<JokeResponse> getJoke() async {
    final _response = await http.get(
      'https://joke.api.gkamelo.xyz/random',
      headers: {
        "x-api-key": "QUtFhHPnlQ5f13LDVpQL3a54XgQzTlCJa1PMSB3o",
        'content-type': 'application/json',
      },
    );
    return _parsedResponse<JokeResponse>(
      _response,
      onSuccess: (r) => JokeResponse.fromResponse(r),
    );
  }
}

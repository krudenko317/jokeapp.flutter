import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jokeapp/models/domain/joke.dart';

class JokeResponse {
  final Joke joke;

  const JokeResponse.success({@required this.joke});
  const JokeResponse.error() : joke = null;

  factory JokeResponse.fromResponse(http.Response response) {
    try {
      final decoded = jsonDecode(response.body) as Map<String, dynamic>;
      final isSuccess = response.statusCode == 200;
      if (isSuccess) {
        return JokeResponse.success(
          joke: Joke.fromMap(decoded),
        );
      } else {
        return const JokeResponse.error();
      }
    } catch (exception, stackTrace) {
      print('JokeResponse parsing ERROR: $exception');
      print(stackTrace);
      return const JokeResponse.error();
    }
  }
}

class Joke {
  final int id;
  final String type;
  final String setup;
  final String punchline;

  Joke({
    this.id,
    this.type,
    this.setup,
    this.punchline,
  });

  factory Joke.fromMap(Map<String, dynamic> map) {
    return Joke(
      id: map['id'] as int,
      type: map['type'] as String,
      setup: map['setup'] as String,
      punchline: map['punchline'] as String,
    );
  }
}

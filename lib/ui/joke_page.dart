import 'package:flutter/material.dart';
import 'package:jokeapp/repositories/api.dart';

class JokePage extends StatefulWidget {
  const JokePage();

  @override
  _JokePageState createState() => _JokePageState();
}

class _JokePageState extends State<JokePage> {
  static const int _durationBeforeInitAnotherJoke = 3;

  String _joke = '';
  String _punchline = '';
  bool _isLoading = true;
  bool _showPunchline = false;

  Future<void> _initJoke() async {
    final _jokeResponse = await Api().getJoke();
    setState(() {
      _joke = _jokeResponse.joke.setup;
      _punchline = _jokeResponse.joke.punchline;
      _isLoading = false;
      _showPunchline = false;
    });
  }

  @override
  void initState() {
    super.initState();
    Future.microtask(_initJoke);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Make me laugh',
        ),
      ),
      body: _isLoading ? _loader() : _jokeArea(),
    );
  }

  Widget _jokeArea() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _setupButton(),
          const SizedBox(
            height: 40,
          ),
          Visibility(
            visible: _showPunchline,
            child: Text(
              _punchline,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  Widget _setupButton() {
    return GestureDetector(
      onTap: () async {
        setState(() {
          _showPunchline = true;
        });
        Future.delayed(const Duration(seconds: _durationBeforeInitAnotherJoke),
            () async {
          setState(() {
            _isLoading = true;
          });
          await _initJoke();
          setState(() {
            _isLoading = false;
          });
        });
      },
      child: Text(
        _joke,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _loader() {
    return const Center(
      child: SizedBox(
        width: 24,
        height: 24,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.blue),
        ),
      ),
    );
  }
}
